import numpy, pmt
P_f32vector = pmt.to_pmt(numpy.array([2.0, 5.0, 5.0, 5.0, 5.0], dtype=numpy.float32))
print(pmt.is_f32vector(P_f32vector)) # Prints 'True'
P_pair = pmt.cons(pmt.string_to_symbol("taps"), P_f32vector)
P_pair
print(pmt.is_pair(P_pair)) # Prints 'true'

# car and cdr allow to deconstruct a pair by accessing the key (car) and values (cdr) of the pair
P_key = pmt.car(P_pair)
P_key # Prints "taps"

P_f32vector2 = pmt.cdr(P_pair)
P_f32vector2
