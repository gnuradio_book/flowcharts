import pmt
key0 = pmt.intern("int")
val0 = pmt.from_long(123)
val1 = pmt.from_long(234)

key1 = pmt.intern("double")
val2 = pmt.from_double(5.4321)
 
# Make an empty dictionary
a = pmt.make_dict()
 
# Add a key:value pair to the dictionary
a = pmt.dict_add(a, key0, val0)
print a
 
# Add a new value to the same key;
# new dict will still have one item with new value
a = pmt.dict_add(a, key0, val1)
print a
 
# Add a new key:value pair
a = pmt.dict_add(a, key1, val2)
print a
 
# Test if we have a key, then delete it
print pmt.dict_has_key(a, key1)
a = pmt.dict_delete(a, key1)
print pmt.dict_has_key(a, key1)
 
ref = pmt.dict_ref(a, key0, pmt.PMT_NIL)
print ref
 
# The following should never print
if (pmt.dict_has_key(a, key0) and pmt.eq(ref, pmt.PMT_NIL)):
    print "Trouble! We have key0, but it returned PMT_NIL"

