#include <fec.h> // gcc -o demo_rs demo_rs.c -I./libfec ./libfec/libfec.a 

// github.com/opensatelliteproject/libsathelper/blob/master/src/reedsolomon.cpp
// dual basis Reed Solomon !
#include "dual_basis.h"

unsigned char pn[255] ={           // randomization polynomial
    0xff, 0x48, 0x0e, 0xc0, 0x9a, 0x0d, 0x70, 0xbc, \
    0x8e, 0x2c, 0x93, 0xad, 0xa7, 0xb7, 0x46, 0xce, \
[...]
    0x08, 0x78, 0xc4, 0x4a, 0x66, 0xf5, 0x58  }; 

#define MAXBYTES (1024) 

#define VITPOLYA 0x4F
#define VITPOLYB 0x6D

#define RSBLOCKS 4

#define PARITY_OFFSET 892
 
void interleaveRS(uint8_t *idata, uint8_t *outbuff, uint8_t pos, uint8_t I) {
  for (int i=0; i < 223; i++) outbuff[i*I+pos]=idata[i];
}

int viterbiPolynomial[2] = {VITPOLYA, VITPOLYB};

int main(int argc,char *argv[]){
 int res, i, j, framebits, fdi, fdo;
 unsigned char data[MAXBYTES], symbols[8*2*(MAXBYTES+6)]; // *8 for bytes->bits & *2 Viterbi
 void *vp;
 int derrors[4] = { 0, 0, 0, 0 };
 uint8_t rsBuffer[255], *tmp;
 uint8_t rsCorData[1020];

 fdi=open("./excerpt.s", O_RDONLY); 
 fdo=open("./output.bin", O_WRONLY|O_CREAT, S_IRWXU|S_IRWXG|S_IRWXO); 
 read(fdi, symbols, 4756+8);  // offset
 framebits = MAXBYTES*8;

 do {
  res = read(fdi, symbols, framebits*2+50);      // 50 additional bytes to finish viterbi decoding
  lseek(fdi, -50, SEEK_CUR);                     // go back 50 bytes
  for (i = 1; i<2*framebits; i+ = 2) symbols[i] = -symbols[i]; // I/Q constellation rotation
  set_viterbi27_polynomial(viterbiPolynomial);
  vp = create_viterbi27(framebits);              // convolution -> Viterbi
  init_viterbi27(vp, 0);
  update_viterbi27_blk(vp, symbols, framebits+6);
  chainback_viterbi27(vp, data, framebits, 0);
  tmp = &data[4];                                // rm synchronization header
  for (i = 0; i<1020; i++) tmp[i]^ = pn[i%255];  // XOR decode (dual basis)

  for (i = 0; i<RSBLOCKS; i++) 
      { for (j = 0;j<255; j++) rsBuffer[j] = tmp[j*4+i]; // deinterleave
        for (j = 0;j<255; j++) rsBuffer[j] = ToDualBasis[rsBuffer[j]];
        derrors[i] = decode_rs_ccsds(rsBuffer, NULL, 0, 0); // decode RS
        for (j = 0; j < 255; j++) rsBuffer[j] = FromDualBasis[rsBuffer[j]];
        interleaveRS(rsBuffer, rsCorData, i, RSBLOCKS);  // interleave
        printf(":%d",derrors[i]);
      }
  write(fdo, data, 4);                      // header
  write(fdo, rsCorData, MAXBYTES-4);  // corrected frame
 } while (res == (2*framebits+50));
 close(fdi);
 close(fdo);
 exit(0);
}

