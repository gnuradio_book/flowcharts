for col = 1:23                              % column number  =  VCDU frame number
  first_head = fin(9, col)*256+fin(10, col) % 70 for column 11
  fin([1:first_head+1]+9, col)';       % beginning of line 11: 1st header in 70
  fin([1:22]+first_head+11, col)';     % start of MCU of line 11
  
  clear l secondary apid m
  l = fin(first_head+16-1, col)*256+fin(first_head+16, col); % vector of packet lengths
  secondary = fin(first_head+16-5, col);     % initializes header list
  apid = fin(first_head+16-4, col);          % initializes APID list
  m = fin([first_head+12:first_head+12+P], col);
  k = 1;
  while ((sum(l)+(k)*7+first_head+12+P)<(1020-128))
    m = [m fin([first_head+12:first_head+12+P]+sum(l)+(k)*7, col)];
    secondary(k+1) = fin(first_head+16+sum(l)+(k)*7-5, col);
    apid(k+1) = fin(first_head+16+sum(l)+(k)*7-4, col);
    l(k+1) = fin(first_head+16-1+sum(l)+(k)*7, col)*256+fin(first_head+16+sum(l)+(k)*7, col); 
           % 16 = offset from VDU beginning
    k = k+1;
  end
  for k = 1:length(l)-1  % saves each MCU bytes in a new file
    jpeg = fin([1:l(k)]+first_head+12+19+sum(l(1:k-1))-1+7*(k-1), col); 
    f = fopen(['jpeg', num2str(apid(k), '%03d'), '_', num2str(col, '%03d'), '_', num2str(k, '%03d'), '.bin'], 'w');
    fwrite(f, jpeg, 'uint8');
    fclose(f);
  end
  
  k = length(l);                                       % last incomplete packet
  jpeg = fin([1+first_head+12+19+sum(l(1:k-1))-1+7*(k-1):end], col);
  first_head = final(9, colonne+1)*256+final(10, col+1); % looks for next VCDU 
  jpeg = [jpeg ; final([1:first_head]+10, col+1)]; 
  % we expected 79 bytes in the last packet: 925-892 = 33 are missing
  f = fopen(['jpeg', num2str(apid(k), '%03d'), '_', num2str(col, '%03d'), '_', num2str(k, '%03d'), '.bin'], 'w');
  fwrite(f, jpeg, 'uint8');
  fclose(f);
end
