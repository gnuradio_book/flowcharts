pkg load communications
M = 32; sps = 8;
% SRRC filter coefficients
h = rcosfir(0.4, [-5*M-M/2 5*M+M/2], sps, 1, 'sqrt');
% Input signal: 1000 dirac pulses
x = repmat([1 zeros(1,sps-1)], 1, 1000);
Nx = length(x);
% Classical upsampling and filtering
tic()
xup = upsample(x,M);
y = filter(h,1,xup);
toc()
% plot the signal
stem(y), hold
tic()
% Polyphase decomposition
xipp = zeros(1, length(x));
% output initialization
for k = 1:M
% Polyphase filtering
xipp(k:M:M*Nx) = filter( h(k:M:end), 1, x ); % k-th polyphase component filtering
end
toc()
% plot the signal
stem(xipp, 'r')

