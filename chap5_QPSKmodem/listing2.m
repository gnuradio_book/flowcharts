clear;
close all;

%Number of samples to simulate
Nsp = 4000;

%Input signal and NCO parameters
w0 = 2*pi*(0.005 + 0.0005);
w0dds = 2*pi*.005;

%Loop parameters
theta = -0.75*pi;
zeta = 0.707;
BnTs = .005;

% Calculate the loop constants. See Rice's book Appendix C (C.58)
K0 = 1; 
KD = 1;
den = 1 + (2*zeta)*(BnTs/(zeta + 1/(4*zeta))) + ...
            (BnTs/((zeta + 1/(4*zeta))))^2;

num = (4*zeta)*(BnTs/(zeta + 1/(4*zeta)));
Kp = num/den;
Kp = Kp/(K0*KD);

num = 4*(BnTs/(zeta + 1/(4*zeta)))^2;
Ki = num/den;
Ki = Ki/(K0*KD);

%Several approximations of the analog PI filter
%b = [ (Kp+Ki) -Kp ];
b = [Kp (Ki-Kp)];
%b = [(Kp + Ki) Ki];
a = [ 1 -1 ];
%a = 1;

in = exp(1j*((1:Nsp)*w0 + theta));
out = zeros(size(in));
error = zeros(size(in));

% Initial conditions
enm1 = 0;
ddsom1 = 0;
ncoout = 1;

for n = 1:Nsp
    %Phase comparator
    pedo = angle(in(n)*conj(ncoout));
    error(n) = pedo;
    
    %Loop filter
    en = pedo*Ki + enm1;
    lpo = pedo*Kp + en;
    
    %DDS
    ddso = lpo*K0 + w0dds + ddsom1;
    
    %Update previous values
    enm1 = en;
    ddsom1 = ddso;
    
    %NCO output
    ncoout = exp(1j*ddso);
    out(n) = ncoout;
end

%% Plot data
figure(1);
plot(1:Nsp, real(in), 'r', 'linewidth', 2), xlabel('Sample number'), ylabel('Amplitude (V)');
hold, plot(1:Nsp, real(out), 'b--', 'linewidth', 2);grid;legend('Input', 'Output');
title('Input and output of the DPLL');
hold off;
figure(2), plot(1:Nsp, error, 'linewidth', 2), grid; title('Evolution of phase error');
xlabel([ '$ \hat{\theta} $ = ' num2str(ddso) ], 'Interpreter', 'latex');
