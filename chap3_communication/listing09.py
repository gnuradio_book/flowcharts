# REQ client
import socket
import zmq
import array
context = zmq.Context()
sock1 = context.socket(zmq.REQ)
sock1.connect("tcp://127.0.0.1:5556");
while True:
 noerror = 1
 while noerror:
  sock1.send(b"Hello")
  rcv = sock1.recv()
  # print(rcv.decode('ascii')) si str
  r = array.array('i', rcv)
  print(f"{len(r)} {r[0]} {r[1]} {r[-1]}")
